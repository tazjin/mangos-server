FROM ubuntu:trusty

# Install build-dependencies
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && apt-get install -y git cmake build-essential\
    libbz2-dev libssl-dev libace-dev libmysql++-dev

# Copy source
ADD . /opt/mangoszero/src

WORKDIR /opt/mangoszero

# Build...
RUN mkdir build && cd build && cmake ../src && make

